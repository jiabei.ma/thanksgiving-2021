"""
This script is used to generate forecasting alpha from processed dataset 9
"""

import pandas as pd
import numpy as np
import os
from functools import reduce
import sys
sys.path.append('..') # Adds higher directory to python modules path.

print(f"Current working dir is {os.getcwd()}\n")
pd.set_option('display.max_columns', None)

# Load utils functions
from math_trend import *
from utils import *

# Settings
ROOT = '/home/jma/Python3.7/Cubist_2021_11' ### please make sure you change root before the run
PROCESSED_DATA_DIR = '/home/jma/Python3.7/Cubist_2021_11/processed_data'

IN_SAMPLE_START_DATE = '2011-01-01'
IN_SAMPLE_END_DATE = '2016-01-01'

NEUTRALIZE_RISK_FACTOR_FLAG = False

dtcol, seccol = 'pos_date', 'security_id'
ds_score = 'd9'



# Signal generation starts from here
try:
    print("Loading processed aggregated dataframe:\n")
    big_df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, 'known_df_on_data_date.csv.gz'),
                         parse_dates=['data_date', 'pos_date', 'ret_date'], dtype={seccol: str},
                         usecols = ['data_date', 'pos_date', 'ret_date', seccol, 'd9', 'fwdret_f1', 'trd_price', 
                                    'volume', 'in_trading_universe', 'sector', 'clip_trisk'])
    assert not big_df.duplicated(subset=[dtcol, seccol]).any(), f"There are duplicated '{dtcol}' '{seccol}'!"
    big_df.sort_values(by=[dtcol, seccol], inplace=True)
    
except Exception as err:
    print(f"Failed to load aggregated dataframe; {err}\n")

print(f"big_df.tail():\n{big_df.tail()}\n")
print(f"big_df.shape:{big_df.shape}\n")
print(f"big_df.describe():\n{big_df.describe()}\n")
print(f"Percentage of valid d9 score for each sector:\n{round(big_df.loc[big_df['d9'].notnull(), 'sector'].value_counts() / len(big_df.dropna(subset=['d9'])) * 100, 2)}")

print(f"Taking out all revelant columns from the DataFrame")
df = big_df.loc[:, ['pos_date', 'security_id', ds_score, 'clip_trisk', 'fwdret_f1', 'sector']].dropna(subset=[ds_score])

print("-	Values outside [0, 100] are clipped to the interval edges")
clipcol = f"clip_{ds_score}"
print(f"-	Using clipped score {clipcol}")
df[clipcol] = df[ds_score].clip(0, 100)

print(f"Percentage of stocks missing values:\n{round(df.isnull().sum() / df.shape[0] * 100, 2)}\n")
print(f"df.shape:{df.shape}\n")

print(f"Computing ztrend scores...\n")
df['z252'] = ztrend(df, clipcol, 252, 22, 'pos_date', seccol)
df['z126'] = ztrend(df, clipcol, 126, 22, 'pos_date', seccol)
df['z63'] = ztrend(df, clipcol, 63, 22, 'pos_date', seccol)

print(f"Replace invalid values with NaNs...\n")
df['z252'] = df['z252'].replace([0, -np.inf, np.inf], np.nan)
df['z126'] = df['z126'].replace([0, -np.inf, np.inf], np.nan)
df['z63'] = df['z63'].replace([0, -np.inf, np.inf], np.nan)

print(f"Strictly only taking out daily tradable universe...\n")
rsch_universe = big_df.loc[big_df['in_trading_universe'] == 'Y', ['data_date', 'pos_date', seccol]].copy()

print(f"Merge alpha_df with tradeable universe\n")
df = df.merge(rsch_universe)

for zcol in ['z63', 'z126', 'z252']:
    print(f"XS normalizing {zcol} score every day within each sectors\n")
    df[f'{zcol}_score'] = wins_qnt_score(df, zcol, 'pos_date', qmin=0.05, qmax=0.95)
    df[f'{zcol}_score'] = std_qnt_score(df, f'{zcol}_score', ['sector', 'pos_date'], demean=True)
    df[f'{zcol}_score'] = df[f'{zcol}_score'].clip(-3, 3)
    df[f'{zcol}_score'] = -df[f'{zcol}_score']

print(f"Checking the descriptive stats and correlation of all three candidate alphas\n")
print(f"Descriptive stats:\n{df[['z63_score', 'z126_score', 'z252_score']].describe()}\n")
print(f"Standard Pearson correlation:\n{df[['z63_score', 'z126_score', 'z252_score']].corr()}\n")

print(f"Taking the simple average of the three candidate alphas\n")
df['combined_score'] = df[['z63_score', 'z126_score', 'z252_score']].mean(axis=1)
print(f"XS normalization every day\n")
df['combined_score'] = std_qnt_score(df, 'combined_score', 'pos_date', demean=True)
print("Clipping the final score\n")
df['combined_score'] = df['combined_score'].clip(-3, 3)


print(f"We have 4 candidate alphas from dataset_9, split the time series into in-sample and out-of-sample")

df_is, df_os = df.loc[df[dtcol].between(IN_SAMPLE_START_DATE, IN_SAMPLE_END_DATE, inclusive=True)], df.loc[df[dtcol]> IN_SAMPLE_END_DATE]

### RUNNING IN-SAMPLE DATA PERFORMANCE for Alpha selection
print("Running analyzer for in-sample z63 alpha:\n")
dret_is_z63, ystats_is_z63 = \
    quickstats(df_is, 'z63_score', 'clip_trisk', riskpower=2, retcol='fwdret_f1', dtcol='pos_date', seccol='security_id')

print("Running analyzer for in-sample z126 alpha:\n")
dret_is_z126, ystats_is_z126 = \
    quickstats(df_is, 'z126_score', 'clip_trisk', riskpower=2, retcol='fwdret_f1', dtcol='pos_date', seccol='security_id')

print("Running analyzer for in-sample z252 alpha:\n")
dret_is_z252, ystats_is_z252 = \
    quickstats(df_is, 'z252_score', 'clip_trisk', riskpower=2, retcol='fwdret_f1', dtcol='pos_date', seccol='security_id')
    
print("Running analyzer for in-sample simple-average alpha:\n")
dret_is_combined, ystats_is_combined = \
    quickstats(df_is, 'combined_score', 'clip_trisk', riskpower=2, retcol='fwdret_f1', dtcol='pos_date', seccol='security_id')

plt.title('Alpha from Dataset_9'); 
plt.legend(['z63_score', 'z126_score', 'z252_score', 'combined_score'], loc='upper left')

print(f"z63_score stats:\n{ystats_is_z63}\n")
print(f"z126_score stats:\n{ystats_is_z126}\n")
print(f"z252_score stats:\n{ystats_is_z252}\n")
print(f"combined_score stats:\n{ystats_is_combined}\n")





