
import pandas as pd
import numpy as np
import os
import sys
from functools import reduce

sys.path.append("..") # Adds higher directory to python modules path.

# settings
ROOT = '/home/jma/Python3.7/Cubist_2021_11' ### please make sure you change ROOT before the run
PROCESSED_DATA_DIR = '/home/jma/Python3.7/Cubist_2021_11/processed_data'

dtcol = 'data_date'
seccol = 'security_id'

DS_FILES = ['data_1.csv.gz', 'data_2.csv.gz', 'data_3.csv.gz', 'data_4.csv.gz',
            'data_5.csv.gz', 'data_6.csv.gz', 'data_7.csv.gz', 'data_9.csv.gz']

TRD_DATE_FILE = 'trd_dates.csv.gz'

RSCH_UNIVERSE_FILE = 'rsch_universe.csv.gz'

ALL_USE_DS = ['d1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd9']

ds_list = []

for ds_file in DS_FILES:
    print(f"Loading processed data '{ds_file}'\n")
    tmp_df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, ds_file), parse_dates=[dtcol], dtype={seccol: str}, compression='gzip')
    assert not tmp_df.duplicated(subset=[dtcol, seccol]).any()
    ds_list.append(tmp_df)

print(f"\nUsing out merge to combine following processed data {DS_FILES}")
all_sigs = reduce(lambda x, y: pd.merge(x, y, on=[dtcol, seccol], how='outer'), ds_list)
all_sigs.sort_values(by=[dtcol, seccol], inplace=True)

print(f"  all_sigs.head():\n{all_sigs.head()}\n")
print(f"  all_sigs.tail():\n{all_sigs.tail()}\n")
print(f"  all_sigs.shape: {all_sigs.shape}")


try:
    print(f"Loading processed data_date, pos_date and ret_date calendats '{TRD_DATE_FILE}'")
    trd_dates = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, TRD_DATE_FILE), parse_dates=['data_date', 'pos_date', 'ret_date'])
    print(f"  trd_dates.head():\n{trd_dates.head()}\n")
    print(f"  trd_dates.tail():\n{trd_dates.tail()}\n")
    print(f"  trd_dates.shape: {trd_dates.shape}")
except Exception as err:
    print(f"Failed loading trd_dates; {err}")


try:
    print(f"Loading processed research universe '{RSCH_UNIVERSE_FILE}'")
    rsch_universe = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, RSCH_UNIVERSE_FILE), 
                                usecols=['data_date', 'security_id', 'close_price', 'volume', 'in_trading_universe',
                                         'ret1d', 'sector', 'clip_trisk'], 
                                parse_dates=['data_date'], dtype={'security_id': str})
    print(f"  rsch_universe.head():\n{rsch_universe.head()}\n")
    print(f"  rsch_universe.tail():\n{rsch_universe.tail()}\n")
    print(f"  rsch_universe.shape: {rsch_universe.shape}\n")
except Exception as err:
    print(f"Failed loading rsch_universe; {err}")


print(f"Appending pos_date and ret_date based on data_date to all_sigs\n")
df = all_sigs.merge(trd_dates)
print(f"  df.head():\n{df.head()}\n")
print(f"  df.tail():\n{df.tail()}\n")
print(f"  df.shape: {df.shape}\n")


ncount = round(df[ALL_USE_DS].isnull().sum() / len(df) * 100, 2)

print(f"Percentage of rows missing values:\n{ncount}\n")

print(f"Descriptive stats for all datasets are:\n{df[ALL_USE_DS].describe()}\n")

print(f"Calculating Spearman Rank Correlation among all used datasets due to the magnitudes are different:\n{df[ALL_USE_DS].corr(method='spearman')}\n")

print("Getting the correct returns for the ret_date\n")
known_df_on_data_date = df.merge(rsch_universe[['data_date', 'security_id', 'ret1d']].rename(
    columns={'data_date': 'ret_date', 'ret1d': 'fwdret_f1'}))


print(f"Getting pos_date, which is used to carry close_price as trade_price data_date is used to compute trisk and volume\n")
known_df_on_data_date = known_df_on_data_date.merge(
    rsch_universe[['data_date', 'security_id', 'close_price']].rename(
        columns={'data_date': 'pos_date', 'close_price': 'trd_price'}))

assert not known_df_on_data_date.duplicated(subset=[dtcol, seccol]).any()
assert not rsch_universe.duplicated(subset=[dtcol, seccol]).any()

print(f"Getting stock's sector, risk information\n")
combined_df = known_df_on_data_date.merge(
    rsch_universe[['data_date', 'security_id', 'volume', 'in_trading_universe', 'sector', 'clip_trisk']],
    on=['data_date', 'security_id'],
    how='inner')

print(f"  combined_df.head():\n{combined_df.head()}\n")
print(f"  combined_df.tail():\n{combined_df.tail()}\n")
print(f"  combined_df.shape: {combined_df.shape}\n")
print(f"  combined_df.columns: {combined_df.columns}\n")

try:
    DATASET_SAVE_PATH = None
    DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, 'known_df_on_data_date.csv.gz')
    combined_df.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    print(f"Saving generated df combining all sigs to {DATASET_SAVE_PATH}\n")
except Exception as err:
    print(f"Failed to save df combining all sigs to {DATASET_SAVE_PATH}, {err}\n")



