"""
This script is used to:
        - pre-processing 11 datasets and 1 security reference data file
        - save the cleaned data to various zipped csv files
"""

import pandas as pd
import numpy as np
import zipfile
import os
import matplotlib.pyplot as plt
import seaborn as sns
import sys
sys.path.append("..") # Adds higher directory to python modules path.

print(f"Current working dir is {os.getcwd()}\n")

from utils import *
from math_trend import *


# Settings
ROOT = '/home/jma/Python3.7/Cubist_2021_11' ### please make sure you change root before the run
PROCESSED_DATA_DIR = '/home/jma/Python3.7/Cubist_2021_11/processed_data'

ADD_TRADING_UNIVERSE = True
ADD_TRADING_CALENDAR = True
ADD_DS_1 = True
ADD_DS_2 = True
ADD_DS_3 = True
ADD_DS_4 = True
ADD_DS_5 = True
ADD_DS_6 = True
ADD_DS_7 = True
ADD_DS_9 = True

# Dataset 8, 10 and 11 are not used in production 
ADD_DS_8 = False   
ADD_DS_10 = False
ADD_DS_11 = False

dtcol, seccol = 'data_date', 'security_id'


if ADD_TRADING_UNIVERSE:
    
    df = None
    rsch_universe = None
    trd_dates = None
    
    print("Loading Security Reference Data...\n")
    with zipfile.ZipFile(os.path.join(ROOT, 'security_reference_data_w_ret1d.zip')) as zf:
        df = pd.read_csv(zf.open('security_reference_data_w_ret1d.csv'), 
                         parse_dates=[dtcol],
                         dtype={seccol: str, 'group_id': 'Int64', 'close_price': np.float64,
                                'volume': 'Int64', 'ret1d': np.float64})
    
    ## to make sure there were no duplicates
    assert not df.duplicated(subset=[dtcol, seccol]).any()
    df.sort_values(by=[dtcol, seccol], inplace=True)
    
    print(f"df.head():\n{df.head()}\n")
    print(f"df.dtypes:\n{df.dtypes}\n")
    print(f"Checking number of NaNs in every column:\n{df.isnull().sum()}\n")
    
    print("Shrinking to in-universe names...\n")
    alldts = df[dtcol].unique()
    allsecs = df.loc[df['in_trading_universe'] == 'Y', seccol].unique()
    print(f"  There are total {len(alldts)} data_date, {len(allsecs)} unique security_id in the raw data.\n\n")

    rsch_universe = pd.merge(pd.DataFrame({'data_date': alldts, 'inuniv': 1}),
                             pd.DataFrame({'security_id': allsecs, 'inuniv': 1}), on=['inuniv'])
    rsch_universe.sort_values(by=['data_date', 'security_id'], inplace=True)
    
    print("Acquiring GICS level-1 sector information...\n")
    assert not df.duplicated(subset=['data_date', 'security_id', 'group_id']).any(), "There are duplicated group_id for certian stocks of some dates!"
    
    rsch_universe = rsch_universe.merge(df, on=['data_date', 'security_id'], how='left')
    
    print(f"  Taking out first 2 digits of group_id and mapping to GICS sectors")
    rsch_universe['sector_id'] = (rsch_universe['group_id'] / 1e6)

    sector_gics_mapping = dict({10: 'Energy', 15: 'Materials', 20: 'Industrials', 25: 'Consumer Discretionary', 
                                30: 'Consumer Staples', 35: 'Health Care', 40: 'Financials', 
                                45: 'Information Technology', 50: 'Communication Services', 55: 'Utilities',
                                60: 'Real Estate'})

    rsch_universe.dropna(subset=['sector_id'], inplace=True)
    rsch_universe['sector_id'] = rsch_universe['sector_id'].astype(int)
    rsch_universe['sector'] = rsch_universe['sector_id'].map(sector_gics_mapping)
    
    del rsch_universe['inuniv']
    
    print("Computing rolling 1 year's realized volatility based on return data")
    ret_ptable = pd.pivot_table(rsch_universe, index=dtcol, columns=seccol, values='ret1d')
    ret_ptable = ret_ptable.sort_index()
    print(f"    Checking few rows:\n{ret_ptable.head().iloc[:10, 100:110]}, '\n\n'")
    
    print(f"    For risk calculation, need to clip the return by +/- 10% to smooth out the spikes")
    ret_ptable = ret_ptable.clip(-0.10, 0.10)
    
    
    print(f"    Computing std using rolling 1 year's data, requiring min 66 days' data.\n")
    vol_ptable = ret_ptable.rolling(252, min_periods=66).std() * np.sqrt(252) * 100

    print(f"    Forward filling volatility by 3 days given risk is normally slow moving.\n")
    vol_ptable.fillna(method='ffill', limit=3, inplace=True)
    
    risk_df = pd.melt(vol_ptable.reset_index(), id_vars=['data_date'], value_name='trisk')
    print(f"    risk_df.head():\n{risk_df.head()}\n")
    print(f"    risk_df.tail():\n{risk_df.tail()}\n")
    print(f"    Checking the distribution of calculated realized risk:\n{risk_df.trisk.describe()}\n")
    
    print(f"    Winsorizing calculated realized risk by lower and upper quantiles\n")
    risk_df['clip_trisk'] = wins_qnt_score(risk_df, 'trisk', 'data_date', qmin=0.05, qmax=0.95)
    
    print(f"   Check the shape of df: {df.shape}\n")
    print(f"   Check the shape of risk_df: {risk_df.shape}\n")
    print(f"   Check the descriptive stats of capped and uncapped versions:\n{risk_df[['trisk', 'clip_trisk']].describe()}\n")
    
    rsch_universe = pd.merge(rsch_universe, risk_df, on=['data_date', 'security_id'], how='left')
    print(f"Check few examples of rsch_universe:\n{rsch_universe.sample(5)}")
    
    try:
        RSCH_UNIVERSE_PATH = os.path.join(PROCESSED_DATA_DIR, 'rsch_universe.csv.gz')
        print(f"Saving generated rsch_universe to {RSCH_UNIVERSE_PATH}")
        rsch_universe.to_csv(RSCH_UNIVERSE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated rsch_universe to {RSCH_UNIVERSE_PATH}, {err}")
        
    if ADD_TRADING_CALENDAR:
        print("Generating trading calendar to properly align data_date, pos_date and ret_date\n\n")
        trd_dates = pd.DataFrame({'data_date': np.sort(rsch_universe['data_date'].unique())})
        trd_dates['pos_date'] = trd_dates['data_date'].shift(-1)
        trd_dates['ret_date'] = trd_dates['data_date'].shift(-2)
        print(trd_dates)
        
        try:
            TRD_DATES_PATH = os.path.join(PROCESSED_DATA_DIR, 'trd_dates.csv.gz')
            print(f"Saving generated trd_dates to {TRD_DATES_PATH}")
            trd_dates.to_csv(TRD_DATES_PATH, index=False, compression='gzip')
        except Exception as err:
            print(f"Failed to save generated rsch_universe to {TRD_DATES_PATH}, {err}")
    

RSCH_UNIVERSE_PATH = '/home/jma/Python3.7/Cubist_2021_11/processed_data/rsch_universe.csv.gz'
print(f"Loading the generated research universe from {RSCH_UNIVERSE_PATH}")
rsch_universe = pd.read_csv(RSCH_UNIVERSE_PATH, compression='gzip', usecols=[dtcol, seccol], parse_dates=[dtcol], dtype={seccol: str})


if ADD_DS_1: 
    
    ds_id = 1
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        

if ADD_DS_2:    
    
    ds_id = 2
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        
        
if ADD_DS_3:    
    
    ds_id = 3
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        
        
        
if ADD_DS_4:    
    
    ds_id = 4
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        
        
if ADD_DS_5:    
    
    ds_id = 5
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        
        
if ADD_DS_6:    
    
    ds_id = 6
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        
        
if ADD_DS_7:    
    
    ds_id = 7
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")
        
        
        
if ADD_DS_9:
    
    ds_id = 9
    
    df = None
    data = None
    DATASET_SAVE_PATH = None
    
    print(f"Loading Dataset {ds_id}...\n")
    scorecol = f'd{ds_id}'
    
    print(f"  De-duplicating, replacing 0 and removing NaNs...\n")
    df = preprocess_dataset(ROOT, ds_id, dtcol=dtcol, seccol=seccol)
    df = replace_zero_and_drop_nan(df, scorecol)
    
    print(f"  Merging the dataset_{ds_id} with research universe...\n")
    data = pd.merge(df, rsch_universe, on=[dtcol, seccol], how='inner')
    print(f"  Daily count of valid values:\n{data.groupby(dtcol).size()}")
    
    try:
        DATASET_SAVE_PATH = os.path.join(PROCESSED_DATA_DIR, f'data_{ds_id}.csv.gz')
        print(f"Saving generated {scorecol} to {DATASET_SAVE_PATH}\n")
        data.to_csv(DATASET_SAVE_PATH, index=False, compression='gzip')
    except Exception as err:
        print(f"Failed to save generated dataset_{ds_id} to {DATASET_SAVE_PATH}, {err}")