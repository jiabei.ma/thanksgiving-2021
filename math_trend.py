import pandas as pd
import numpy as np

def ztrend(df, scorecol, lookback, mindata, dtcol, seccol):
    """
    Compute the time series ztrended score for each seccol
    """
    
    assert not df.duplicated(subset=[dtcol, seccol]).any()
    
    df = df.copy()
    df.sort_values(by=[dtcol, seccol], inplace=True)
    
    ptable = pd.pivot_table(df, index=dtcol, columns=seccol, values=scorecol)
    
    mu_ptable = ptable.rolling(window=lookback, min_periods=mindata).mean()
    sigma_ptable = ptable.rolling(window=lookback, min_periods=mindata).std()
    
    zscore_ptable = (ptable - mu_ptable) / sigma_ptable
    df_zscore = pd.melt(zscore_ptable.reset_index(), id_vars=[dtcol])
    df = df.merge(df_zscore, on=[dtcol, seccol], how='left')
    
    return df['value'].values


def wins_qnt_score(df, scorecol, dtcol, qmin=0.05, qmax=0.95):
    """
    winsorizing scorecol by upper and lower quantiles cross-sectionally 
    """
    
    def _helper(x):
        l, u = np.nanquantile(x, qmin), np.nanquantile(x, qmax)
        x[x > u] = u
        x[x < l] = l
        return x
    return df.groupby(dtcol)[scorecol].transform(_helper)


def std_qnt_score(df, scorecol, dtcol, demean=True):
    """
    demean and standardizing scorecol 
    """
    
    if demean:
        return df.groupby(dtcol)[scorecol].transform(lambda x: (x - x.mean()) / x.std())
    else:
        return df.groupby(dtcol)[scorecol].transform(lambda x: x / x.std())