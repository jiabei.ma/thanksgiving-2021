import pandas as pd
import numpy as np
import zipfile
import os
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm


from functools import reduce

plt.style.use("ggplot")
plt.rcParams['figure.figsize'] = [15, 7]


def preprocess_dataset(data_root, ds_id, dtcol='data_date', seccol='security_id'):
    
    assert type(ds_id) == int
    ds_filename = f'data_set_{ds_id}.csv'
    scorecol = f'd{ds_id}'
    
    df = read_raw_dataset(data_root, ds_filename, scorecol, dtcol=dtcol, seccol=seccol, verbose=True)
    
    ### Check whether there are duplicated dtcol and seccol pairs 
    if df.duplicated(subset=[dtcol, seccol]).any():
        df = remove_duplicated_key_pairs(df, scorecol, dtcol=dtcol, seccol=seccol, avg_min_cut=1.1, avg_max_cut=0.9, verbose=True)
    else:
        print("No duplicated dtcol-seccol pairs is found.\n")

    print(f"shape of df {df.shape}.\n")
    print(df.dtypes)
    
    return df
       
    

def read_raw_dataset(data_root, ds_filename, scorecol, dtcol='data_date', seccol='security_id', verbose=True):
    """
    This function is used to read in the data from data_sets.zip file
    """
    print(f"Getting data from file '{ds_filename}' for score '{scorecol}'.\n")

    with zipfile.ZipFile(os.path.join(data_root, 'data_sets.zip')) as zf:
        df = pd.read_csv(zf.open(ds_filename), parse_dates=[dtcol], dtype={seccol: str, scorecol: np.float64})
    
    df[scorecol] = df[scorecol].apply(lambda x: pd.to_numeric(x, errors='coerce'))
    df.sort_values(by=[dtcol, seccol], inplace=True)
    
    if verbose:
        print(f"df.head():\n{df.head()}\n")
        print(f"df.tail():\n{df.tail()}\n")
        print(f"getting df shape of {df.shape}\n")
        print(f"df.dtypes:\n{df.dtypes}\n")
    
    return df


def remove_duplicated_key_pairs(df, scorecol, dtcol='data_date', seccol='security_id', avg_min_cut=1.1, avg_max_cut=0.9, verbose=True):
    """
    This function is used to remove duplicates and compute the 'true' values for each (date + security) pairs
    """
    
    df = df.copy()
    
    if verbose:
        print(f"WARNING! There are duplicated {dtcol} and {seccol} pairs! There are total {df.duplicated(subset=[dtcol, seccol]).sum()} duplicated rows")
        print("  Will follow pre-defined logics to remove bad data and get the average value")
    
    min_df = df.groupby([dtcol, seccol], as_index=False)[scorecol].min()
    max_df = df.groupby([dtcol, seccol], as_index=False)[scorecol].max()
    avg_df = df.groupby([dtcol, seccol], as_index=False)[scorecol].mean()

    avg_min_max = reduce(lambda x, y: pd.merge(x, y, on=[dtcol, seccol]),
                        [min_df.rename(columns={scorecol: f'min_{scorecol}'}),
                         max_df.rename(columns={scorecol: f'max_{scorecol}'}),
                         avg_df.rename(columns={scorecol: f'avg_{scorecol}'})])
    
    # avg_min_max.head()
    avg_min_max['avg_min_pct'] = avg_min_max[f'avg_{scorecol}'] / avg_min_max[f'min_{scorecol}']
    avg_min_max['avg_max_pct'] = avg_min_max[f'avg_{scorecol}'] / avg_min_max[f'max_{scorecol}']

    print(avg_min_max.describe().round(4))
    
    avg_min_max = avg_min_max.loc[(avg_min_max['avg_min_pct'] <= avg_min_cut) & (avg_min_max['avg_max_pct'] >= avg_max_cut)]
    df = avg_min_max[[dtcol, seccol, f'avg_{scorecol}']].rename(columns={f'avg_{scorecol}': scorecol})
    
    assert not df.duplicated(subset=[dtcol, seccol]).any()
    
    return df


def replace_zero_and_drop_nan(df, scorecol):
    
    df = df.copy()
    print("Since 0 values normally carry useless information in type of sparse dataset, thus we replace 0 with NaN and drop them")
    df[scorecol] = df[scorecol].replace([0, np.inf, -np.inf], np.nan)
    print(f"NaNs count for each column:\n{df.isnull().sum()}")
    
    print("Dropping all NaNs")
    df.dropna(subset=[scorecol], inplace=True)
    print(f"descriptive stats for df is:\n{df.describe()}")
    
    return df


def plot_daily_stock_count(df, scorecol, dtcol='data_date'):
    df.groupby(dtcol)[scorecol].apply(
        lambda x: pd.Series([x.notnull().sum(), x.isnull().sum(), x.notnull().sum() / x.shape[0] * 100],
                            index=[f'Daily number of stocks with valid score {scorecol}',
                                   f'Daily number of stocks with NaN score {scorecol}',
                                   f'Daily % of stocks with valid score {scorecol}'])).unstack().plot(subplots=True)


def plot_timeseries_dist(df, scorecol, dtcol, plot_min_max=False):
    """
    This function is used to plot time series percentiles [20%, 40%, 60%, 80%] of given score column, including min and max if required
    """
    if plot_min_max:
        ts_series = df.groupby(dtcol)[scorecol].apply(
            lambda x: pd.Series(
                [x.min(), x.quantile(0.2), x.quantile(0.4), x.quantile(0.6), x.quantile(0.8), x.max()],
                 index=['Min', '20%', '40%', '60%', '80%', 'Max']))
    else:
        ts_series = df.groupby(dtcol)[scorecol].apply(
            lambda x: pd.Series(
                [x.quantile(0.2), x.quantile(0.4), x.quantile(0.6), x.quantile(0.8)],
                 index=['20%', '40%', '60%', '80%']))
        
    ts_series.unstack().plot()    

    
def quickstats(df, scorecol, riskcol, riskpower=2, retcol='fwdret_f1', dtcol='pos_date', seccol='security_id'):
    """
    This function is used to generate some useful factor portfolio statistics 
    """
    
    df = df.copy()
    
    ### get portfolio performance stats
    df['hld'] = df[scorecol] / np.power(df[riskcol], riskpower)
    df['hld_wgt'] =df.groupby(dtcol)['hld'].transform(lambda x: x / x.abs().sum())
    
    df['attr_fwdretf1'] = df['hld_wgt'] * df[retcol]
    xxx = df.groupby(dtcol, as_index=False)['attr_fwdretf1'].apply(lambda x: x.sum())
    xxx['year'] = xxx['pos_date'].dt.year
    
    ann_stats = xxx.groupby('year')['attr_fwdretf1'].apply(
        lambda x: pd.Series([x.mean() * len(x), x.std() * np.sqrt(len(x)), x.shape[0]],
                            index=['AnnRet', 'AnnVol', 'Days'])).unstack()
    ann_stats['AnnSR'] = ann_stats['AnnRet'] / ann_stats['AnnVol']
    
    ## compute averge turnover per year
    daily_to = calc_daily_to(df, 'hld_wgt', dtcol, seccol)
    daily_to = daily_to.reset_index()
    daily_to['year'] = daily_to[dtcol].dt.year
    ann_to = daily_to.groupby('year').mean()
    ann_to.columns = ['AnnTO']
    
    ### compute averge number of stocks per year
    ncount = df.groupby(dtcol, as_index=False)[scorecol].size()
    ncount['year'] = ncount[dtcol].dt.year
    ncount = ncount.groupby('year').mean().astype(int)
    ncount.columns = ['Count']
    
    xxx.set_index(dtcol)['attr_fwdretf1'].cumsum().plot(title=f'Cumulative portfolio return')
    return xxx.drop(['year'], axis=1), pd.concat([ann_stats.round(4), ncount, ann_to.round(4)], axis=1)


def calc_daily_to(df, hldcol, dtcol, seccol):
    """
    This function is used to compute portfolio daily turnover rate
    """
    ptable = pd.pivot_table(df, index=dtcol, columns=seccol, values=hldcol)
    ptable.fillna(0, inplace=True)
    daily_to = ptable.diff(1).abs().sum(axis=1) / ptable.shift(1).abs().sum(axis=1)
    return daily_to


def calc_drawdown(daily_return):
    """
    This function is used to compute max drawdown
    """
    xs = daily_return.cumsum()
    i = np.argmax(np.maximum.accumulate(xs) - xs) # end of the period
    j = np.argmax(xs[:i]) # start of period
    
    return pd.Series([round(xs[i] - xs[j], 3), i - j],
                     index=['Max_Drawdown', 'Length'])



def orth_by_day(X, y):
    """
    This function is used to orthogonalize dependent series to independent dataframe
    """
    model = sm.OLS(y, X)
    results = model.fit()
    return results.resid, results.rsquared_adj

def orthogonalize(df, dep_col, ind_cols, fit_intercept=False, dtcol='pos_date'):
    
    df = df.copy()
    alldts = df[dtcol].unique()
    
    l = []
    
    for dt in alldts:
        dt = pd.Timestamp(dt)
        print(f"Processing date {dt}")
        y = df.loc[df[dtcol] == dt, dep_col].copy()
        X = df.loc[df[dtcol] == dt, ind_cols].copy()
        if fit_intercept:
            X = sm.add_constant(X)
        resid, rsquare_adj = orth_by_day(X, y)
        print(f"    Fitted adjusted R^2 is {rsquare_adj}")
        l.append(resid)
    df['Resid'] = pd.concat(l)
    return df
    